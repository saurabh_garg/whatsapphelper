package com.til.whatsapphelper.helper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.til.whatsapphelper.MainActivity;
import com.til.whatsapphelper.accessibility.WhatsAppAccessibilityService;

public class Utils {

    public static void showToast(Context context, CharSequence message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void logDebug(String message) {
        logDebug("Saurabh", message);
    }

    public static void logDebug(String tag, String message) {
        Log.d(tag, message);
    }

    public static void logException(Exception ex) {
        ex.printStackTrace();
    }

    public static void sendWhatsAppMessage(Context context, String mobileNumber, String message) {
        try {
            if (!TextUtils.isEmpty(mobileNumber)) {
                mobileNumber = getUpdatedWhatsAppNumber(mobileNumber);
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                sendIntent.putExtra("jid", mobileNumber);
                sendIntent.setPackage("com.whatsapp");
                context.startActivity(sendIntent);
            }
        } catch (Exception e) {
            logException(e);
        }
    }

    private static String getUpdatedWhatsAppNumber(@NonNull String mobileNumber) {
        if (!TextUtils.isEmpty(mobileNumber) && mobileNumber.length() == 10) {
            mobileNumber = "91" + mobileNumber + "@s.whatsapp.net";
        }
        return mobileNumber;
    }

    public static void startAccessibilityService(Context context) {
        Intent intent = new Intent(context, WhatsAppAccessibilityService.class);
        context.startService(intent);
    }
}
