package com.til.whatsapphelper;

import android.app.Activity;
import android.os.Bundle;

import com.til.whatsapphelper.helper.Utils;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils.startAccessibilityService(this);
    }
}
