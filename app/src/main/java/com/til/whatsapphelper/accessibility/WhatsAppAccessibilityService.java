package com.til.whatsapphelper.accessibility;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Handler;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.til.whatsapphelper.helper.Utils;

import java.util.List;

public class WhatsAppAccessibilityService extends AccessibilityService {
    private boolean isEventInProcess = false;
    private boolean isHandlerInProcess = false;

    @Override
    public void onServiceConnected() {
        Utils.logDebug("AccessibilityService", "onServiceConnected");
        // Set the type of events that this service wants to listen to. Others won't be passed to this service.
        // We are only considering windows state changed event.
        AccessibilityServiceInfo info = getServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_WINDOWS_CHANGED
                | AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED
                | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
                | AccessibilityEvent.TYPE_VIEW_HOVER_ENTER | AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_START | AccessibilityEvent.TYPE_VIEW_FOCUSED;
        // If you only want this service to work with specific applications, set their package names here. Otherwise, when the service is activated, it will listen to events from all applications.
        info.packageNames = new String[]{"com.whatsapp"};
        // Set the type of feedback your service will provide. We are setting it to GENERIC.
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        // Default services are invoked only if no package-specific ones are present for the type of AccessibilityEvent generated.
        // This is a general-purpose service, so we will set some flags
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;
        info.flags = AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS;
        info.flags = AccessibilityServiceInfo.FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY;
        info.flags = AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS;
        // We are keeping the timeout to 0 as we don’t need any delay or to pause our accessibility events
        info.notificationTimeout = 0;
        this.setServiceInfo(info);
    }

    @Override
    public void onInterrupt() {
        Utils.logDebug("AccessibilityService", "onInterrupt");
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityNodeInfo rootInActiveWindow = getRootInActiveWindow();
        if (rootInActiveWindow == null || accessibilityEvent.getSource() == null) {
            return;
        }
        List<AccessibilityNodeInfo> nodeInfoList = rootInActiveWindow.findAccessibilityNodeInfosByViewId("com.whatsapp:id/send");
        handleAccessibilityNodeView(nodeInfoList);
    }

    private void handleAccessibilityNodeView(List<AccessibilityNodeInfo> nodeInfoList) {
        if (nodeInfoList != null && nodeInfoList.size() > 0) {
            for (AccessibilityNodeInfo nodeInfo : nodeInfoList) {
                showView(nodeInfo);
            }
        }
    }

    private void showView(AccessibilityNodeInfo nodeInfo) {
        if (nodeInfo == null) {
            return;
        }
        if (nodeInfo.getChildCount() > 0) {
            int count = nodeInfo.getChildCount();
            for (int i = 0; i < count; i++) {
                showView(nodeInfo.getChild(i));
            }
        }
        if ("android.widget.ImageButton".equals(nodeInfo.getClassName())) {
            performAction(nodeInfo);
        }
    }

    private void performAction(final AccessibilityNodeInfo nodeInfo) {
        if (!isEventInProcess && nodeInfo != null) {
            isEventInProcess = true;
            Utils.logDebug("AccessibilityService", "performAction");
            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK);
            changeEventInProcess();
        }
    }

    private void changeEventInProcess() {
        if (!isHandlerInProcess) {
            isHandlerInProcess = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isHandlerInProcess = false;
                    isEventInProcess = false;
                }
            }, 1000);
        }
    }
}
