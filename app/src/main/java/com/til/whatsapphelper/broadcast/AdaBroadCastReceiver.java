package com.til.whatsapphelper.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.til.whatsapphelper.helper.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class AdaBroadCastReceiver extends BroadcastReceiver {
    private static final String ADB_ACTION = "com.whatsappHelper.intent";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (context != null && intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            String data = bundle.getString("data");
            handleIntentData(context, data);
        }
    }

    private void handleIntentData(Context context, String data) {
        try {
            logData(context, data);
            if (!TextUtils.isEmpty(data)) {
                JSONObject jsonObject = new JSONObject(data);
                String mobileNumber = jsonObject.optString("mobile");
                String message = jsonObject.optString("message");
                Utils.sendWhatsAppMessage(context, mobileNumber, message);
            }
        } catch (JSONException e) {
            Utils.logException(e);
        }
    }

    private void logData(Context context, String data) {
        String toastMessage = "Incoming: " + data;
        Utils.showToast(context, toastMessage);
        Utils.logDebug(toastMessage);
    }
}
